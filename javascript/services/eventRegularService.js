var complaints = [ {
    subject: "Not good",
	date: "2017-11-11",
	loc: "Yard",
    description: "Wet floor",
    user: "Bob Dylan"
}];

function addComplaint(complaint) {
    complaints.push(complaint);
}

function getAllComplaints() {
    return complaints;
}
