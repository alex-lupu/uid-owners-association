var complaints = [{
    title: "Quiet hours",
    description: "Between 14:00 - 16:00",
    priority: "others",
    user: "John Doe"
}, {
    title: "Disturbing neighbours",
    description: "I would be glad if you could talk to neighbours on nr 35.They listen to loud music every evening",
    priority: "medium",
    user: "John Doe"
}];

function addComplaint(complaint) {
    complaints.push(complaint);
}

function getAllComplaints() {
    return complaints;
}
