$(document).ready(function () {
    populateComplaintsTable();
    addComplaintButtonEventHandler();
    saveComplaintEventHandler();
    confirmationDialogEventHandler();
});

function saveComplaintEventHandler() {
    $("#save-complaint").click(function () {
        var type = $("input[name='type']:checked").val();
        var title = $("#title").val();
        var description = $("#description").val();
        var priority = $('#priority :selected').text();
        if (isComplaintValid(type, title, description) == true) {
            saveComplaint(type, title, description, priority);
            $("#complaintModal").modal("hide");
            successNotification();
        }
        else {
            $("#complaint-error-message").text("All fields must be completed!");
        }
    });
}

function successNotification() {
    var body = $("body");
    var div = $("<div>").addClass("notification-div").appendTo(body);
    $("<strong>").text("Success!").appendTo(div);
    div.addClass("alert alert-success alert-dismissable").text("You successfully added a new complaint");

    setTimeout(function () {
        $(".notification-div").fadeOut().empty();
    }, 3000);
}

function addComplaintButtonEventHandler() {
    $("#btn-add-complaint").click(function () {
        clearModal();
    });
}

function confirmationDialogEventHandler() {
    $("#btn-yes-confirmation").click(function () {
        $("#complaintModal").modal("hide");
    });
}

function saveComplaint(type, title, description, priority) {
    var complaint = {
        type: type,
        title: title,
        description: description,
        priority: priority,
        user: getLoggedUserName()
    }
    addComplaint(complaint);
    populateComplaintsTable();
}

function clearModal() {
    $("#complaint-error-message").text("");
    $("#description").val("");
    $("#title").val("");
    $("input[name='type']").prop("checked", false);
    $("#complaint-type").removeClass("error-border");
    $("#title").removeClass("error-border");
    $("#description").removeClass("error-border");
}

function isComplaintValid(type, title, description) {
    var returnedValue = true;
    if (type == null) {
        $("#complaint-type").addClass("error-border");
        returnedValue = false;
    }
    else {
        $("#complaint-type").removeClass("error-border");
    }
    if (title.length === 0) {
        $("#title").addClass("error-border");
        returnedValue = false;
    }
    else {
        $("#title").removeClass("error-border");
    }
    if (description.length === 0) {
        $("#description").addClass("error-border");
        returnedValue = false;
    }
    else {
        $("#description").removeClass("error-border");
    }
    return returnedValue;
}

function populateComplaintsTable() {
    var tableBody = $("#body-complaints");
    var complaints = getAllComplaints();
    tableBody.empty();

    $.each(complaints, function (key, item) {
        var tr = $("<tr>").appendTo(tableBody);
        $("<td>").text(item.title).appendTo(tr);
        $("<td>").text(item.description).appendTo(tr);
        $("<td>").text(item.type).appendTo(tr);
        $("<td>").text(item.user).appendTo(tr);
        var priorityClass = getClassForPriorityLevel(item.priority);
        $("<td>").text(item.priority).attr("class", priorityClass).appendTo(tr);
    });
}

function getClassForPriorityLevel(priorityLevel) {
    if (priorityLevel == "low") {
        return "btn-success";
    }
    if (priorityLevel == "medium") {
        return "btn-warning";
    }
    if (priorityLevel == "high") {
        return "btn-danger";
    }
}