var users = [{
    username: "admin",
    password: "admin",
    role: "admin"
}, {
    username: "user",
    password: "user",
    number: 34,
    role: "regular",
    blockId: 1,
    taxes: []
}, {
    username: "John Doe",
    password: "user",
    number: 12,
    role: "regular",
    blockId: 1,
    taxes: [{
        month: "November 2017",
        water: 125,
        perCapita: 16,
        illumination: 5,
        cleaning: 2,
        status: "OK"
    },
    {
        month: "December 2017",
        water: 130,
        perCapita: 16,
        illumination: 4,
        cleaning: 3,
        status: "NO"
    }]
}, {
    username: "Billy Cash",
    password: "user",
    number: 2,
    role: "regular",
    blockId: 1,
    taxes: [{
        month: "November 2017",
        water: 105,
        perCapita: 16,
        illumination: 5,
        cleaning: 2,
        status: "OK"
    },
    {
        month: "December 2017",
        water: 100,
        perCapita: 16,
        illumination: 4,
        cleaning: 3,
        status: "NO"
    }]
}, {
    username: "Bob Dylan",
    password: "user",
    number: 24,
    role: "regular",
    blockId: 1,
    taxes: [{
        month: "November 2017",
        water: 127,
        perCapita: 16,
        illumination: 5,
        cleaning: 2,
        status: "OK"
    },
    {
        month: "December 2017",
        water: 120,
        perCapita: 16,
        illumination: 4,
        cleaning: 3,
        status: "OK"
    }]
}, {
    username: "Diana Gary",
    password: "user",
    number: 5,
    role: "regular",
    blockId: 1,
    taxes: [{
        month: "November 2017",
        water: 155,
        perCapita: 16,
        illumination: 5,
        cleaning: 2,
        status: "OK"
    },
    {
        month: "December 2017",
        water: 140,
        perCapita: 16,
        illumination: 4,
        cleaning: 3,
        status: "OK"
    }]
}, {
    username: "Alan Stancey",
    password: "user",
    number: 18,
    role: "regular",
    blockId: 1,
    taxes: [{
        month: "November 2017",
        water: 105,
        perCapita: 16,
        illumination: 5,
        cleaning: 2,
        status: "OK"
    },
    {
        month: "December 2017",
        water: 122,
        perCapita: 16,
        illumination: 6,
        cleaning: 3,
        status: "NO"
    }]
}, {
    username: "El Lanch",
    password: "user",
    number: 34,
    role: "regular",
    blockId: 2,
    taxes: [{
        month: "November 2017",
        water: 105,
        perCapita: 16,
        illumination: 5,
        cleaning: 2,
        status: "OK"
    },
    {
        month: "December 2017",
        water: 122,
        perCapita: 16,
        illumination: 6,
        cleaning: 3,
        status: "NO"
    }]
}];

function addUser(user) {
    users.push(user);
}

function findByUsernameAndPassword(username, password) {
    var user = null;
    $.each(users, function (key, item) {
        if (item.password == password && item.username == username) {
            user = item;
            localStorage.setItem("loggedUserName", user.username);
        }
    });
    return user;
}

function getLoggedUserName() {
    return localStorage.getItem("loggedUserName");
}

function getUsersFromBlock(blockId) {
    var usersFromBlock = [];
    $.each(users, function (key, item) {
        if (item.role === "regular") {
            if (item.blockId == blockId) {
                usersFromBlock.push(item);
            }
        }
    });
    return usersFromBlock;
}

function getAllUsers() {
    return users;
}

function addTax(userNumber, tax) {
    $.each(users, function (key, item) {
        if (item.number == userNumber) {
            item.taxes.push(tax);
        }
    });
}

function getTaxesOf(username) {
    var taxes;
    $.each(users, function (key, item) {
        if(item.username === username) {
            taxes=item.taxes;
        }
    });
    return taxes;
}

function payTax(userNumber, taxIndex) {
    $.each(users, function(key, item) {
        if(item.number == userNumber) {
            var tax = item.taxes[taxIndex];
            tax.status="OK";
            return;
        }
    })
}