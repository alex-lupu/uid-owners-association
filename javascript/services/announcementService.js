const allUsers = getAllUsers();

let announcements = [
	{
		username: allUsers[4].username,
		announcement_title: 'Lorem',
		announcement: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with ',
		date: moment().subtract(129,'d').format('DD.MM.YYYY'),
		new: false
	},
	{
		username: allUsers[3].username,
		announcement_title: 'Party',
		announcement: 'I would like to have a party tonight, anyone who would like to come - tha party starts around 7.',
		date: moment().subtract(7,'d').format('DD.MM.YYYY'),
		new: true
	},
	{
		username: allUsers[2].username,
		announcement_title: 'Apartment for rent',
		announcement: 'I would like to give my apartment for rent. Anyone who is interested or knows someone that might be interested, please contact me.',
		date: moment().format('DD.MM.YYYY'),
		new: true
	}
];


const writeDataToLocalStorage = () => {
	localStorage.setItem('announcementsData', JSON.stringify(announcements))
}

const saveAnnouncementEntry = entry => {
	// add new one
	announcements.push(entry);
	// write to storage - hacky but it's the fastest i could think of rn
	writeDataToLocalStorage();
}

const setAnnouncementsEntries = ( ) => {
	// delete if accidentally there was something else before
	// merge mockup local ones with the ones in local storage
	_.merge(announcements, getAnnouncementsEntries());
	// localStorage.removeItem('waterConsumptionData');
	// Put the object into storage
	writeDataToLocalStorage();
	// Retrieve the object from storage
}

const getUserAnnouncements = ( username ) => {
	return getAnnouncementsEntries().filter( entry => entry.username === username );
};

const getAnnouncementsEntries = () => {
	return JSON.parse(localStorage.getItem('announcementsData'));
};