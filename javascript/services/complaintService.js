var complaints = [{
    type: "problem",
    title: "Poor comunication",
    description: "We should make more meetings",
    priority: "low",
    user: "John Doe"
}, {
    type: "complain",
    title: "Disturbing neighbours",
    description: "I would be glad if you could talk to neighbours on nr 35.They listen to loud music every evening",
    priority: "medium",
    user: "John Doe"
}, {
    type: "complain",
    title: "Noisy party",
    description: "Some young girls made a noisy party.",
    priority: "low",
    user: "John Doe"
}, {
    type: "problem",
    title: "Broken pipes",
    description: "This morning the pipes in the bathroom were broken.",
    priority: "low",
    user: "John Doe"
}, {
    type: "problem",
    title: "Unacceptable problem",
    description: "Wet floor",
    priority: "high",
    user: "Bob Dylan"
}];

function addComplaint(complaint) {
    complaints.push(complaint);
}

function getAllComplaints() {
    return complaints;
}
