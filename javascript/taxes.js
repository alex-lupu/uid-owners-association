$(document).ready(function () {
    populateTaxesTable();
    saveTaxEventHandler();
    confirmationDialogEventHandler();
});

function saveTaxEventHandler() {
    $("#add-tax").click(function () {
        var month = $("#month :selected").text() + "2018";
        var water = $("#water").val();
        var perCapita = $("#per-capita").val();
        var cleaning = $("#cleaning").val();
        var illumination = $("#illumination").val();
        var userNumber = localStorage.getItem("user-to-add-tax");
        if (isTaxValid(month, water, perCapita, cleaning, illumination) == true) {
            saveTax(userNumber, month, water, perCapita, cleaning, illumination);
            $("#addTaxModal").modal("hide");
            successNotification();
        }
        else {
            $("#tax-error-message").text("All fields must be completed!");
        }
    });
}

function successNotification() {
    var body = $("body");
    var div = $("<div>").addClass("notification-div").appendTo(body);
    $("<strong>").text("Success!").appendTo(div);
    div.addClass("alert alert-success alert-dismissable").text("You successfully added a new tax");

    setTimeout(function () {
        $(".notification-div").fadeOut().empty();
    }, 3000);
}

function confirmationDialogEventHandler() {
    $("#btn-yes-confirmation").click(function () {
        $("#addTaxModal").modal("hide");
        clearModal();
    });
}


function saveTax(userNumber, month, watter, perCapita, cleaning, illumination) {
    var tax = {
        month : month,
        water : watter,
        perCapita : perCapita,
        cleaning : cleaning,
        illumination : illumination,
        status : "NO"
    }
    addTax(userNumber, tax);
    populateTaxesTable();
    clearModal();
}

function clearModal() {
    $("#tax-error-message").text("");
    $("#water").val(0);
    $("#per-capita").val(0);
    $("#cleaning").val(0);
    $("#illumination").val(0);
}

function isTaxValid(month, water, perCapita, cleaning, illumination) {
    var returnedValue = true;
    if (water < 0) {
        $("#water").addClass("error-border");
        returnedValue = false;
    }
    else {
        $("#complaint-type").removeClass("error-border");
    }
    if (perCapita < 0) {
        $("#per-capita").addClass("error-border");
        returnedValue = false;
    }
    else {
        $("#per-capita").removeClass("error-border");
    }
    if (cleaning < 0) {
        $("#cleaning").addClass("error-border");
        returnedValue = false;
    }
    else {
        $("#cleaning").removeClass("error-border");
    }

    if (illumination < 0) {
        $("#illumination").addClass("error-border");
        returnedValue = false;
    }
    else {
        $("#illumination").removeClass("error-border");
    }

    return returnedValue;
}

function populateTaxesTable() {
    var tableBody = $("#body-taxes");
    var users = getAllUsers();
    tableBody.empty();

    $.each(users, function (key, item) {
        if(item.role === "regular") {
            var tr = $("<tr>").appendTo(tableBody);
            $("<td>").text(item.username).appendTo(tr);
            $("<td>").text("Izlazului nr." + item.blockId).appendTo(tr);
            $("<td>").text(getTaxesStatus(item.taxes)).appendTo(tr);
            var td = $("<td>").appendTo(tr);
            $("<a>").attr("type", "button")
                    .attr("class", "btn btn-info")
                    .attr("data-toggle", "modal")
                    .attr("data-target", "#addTaxModal")
                    .text("+").appendTo(td)
                    .click(function(){
                        localStorage.setItem("user-to-add-tax", item.number);
                    });

        }
    });
}

function getTaxesStatus(taxes) {
    var isUserUpToDate = true;
    $.each(taxes, function (key, item) {
        if(item.status === "NO") {
            isUserUpToDate = false;
        }
    });
    if (isUserUpToDate) {
        return "OK"
    } else {
        return "OVERDUE!"
    }
}

function getClassForPriorityLevel(priorityLevel) {
    if (priorityLevel == "low") {
        return "btn-success";
    }
    if (priorityLevel == "medium") {
        return "btn-warning";
    }
    if (priorityLevel == "high") {
        return "btn-danger";
    }
}