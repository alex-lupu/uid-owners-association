$(document).ready(function () {
	setWaterConsumptionEntries()
	populateWaterConsumptionTable();
	popupHandler();
	updateEntryEvHandler();
});



// debugger;

// add event handler to populate modal when edit button gets clicked
const popupHandler = () =>{ $(document).on("click", ".water-entry-edit-button", function () {
	var bathroomConsumption = $(this).data('bathroom-consumption'),
		kitchenConsumption = $(this).data('kitchen-consumption'),
		comment = $(this).data('comment'),
		month = $(this).data('month');

	$(".modal-body #bathroom").val( bathroomConsumption );
	$(".modal-body #kitchen").val( kitchenConsumption );
	$(".modal-body #comment").val( comment );
	$(".modal-body #month").val( month );
})
};

const isWaterEntryValid = ( bathroomConsumption, kitchenConsumption ) => {
	var isValid = true;
	if( bathroomConsumption == null || '') {
		$("#bathroom").addClass("error-border");
		isValid = false
	}
	if( kitchenConsumption == null || '') {
		$("#kitchen").addClass("error-border");
		isValid = false
	}
	return isValid;
}

const updateEntryEvHandler = () => {
	$("#update-water-entry").click(function () {
		var bathroomConsumption = $("input[name='bathroom']").val(),
			kitchenConsumption = $("input[name='kitchen']").val(),
			month = $("input[name='month']").val(),
			comment = $("input[name='comment']").val(),
			waterEntryConsumption = {
				month: month,
				bathroom_consumption: Number(bathroomConsumption),
				kitchen_consumption: Number(kitchenConsumption),
				editable: true,
				comment: comment
			};
		if (isWaterEntryValid(bathroomConsumption, kitchenConsumption)) {
			updateWaterConsumptionEntry(waterEntryConsumption);
			populateWaterConsumptionTable();
			$("#waterEntryModal").modal('toggle');
		}
	});
}

const populateWaterConsumptionTable = () => {
	const tableBody = $("#body-waterconsumptions");
	tableBody.empty();
	const editBtn = $('<a type="button" class="btn btn-primary water-entry-edit-button" style="float:right" id="btn-edit-water-entry" data-toggle="modal" data-target="#waterEntryModal">Edit</a>');

	let waterConsumptionEntries = getWaterConsumptionEntries();
	let waterConsumptionEntriesReversed = waterConsumptionEntries.reverse();

	const getTotalConsumptionEl = ( waterEntry, editable ) => {
		var totalConsumptionValue = waterEntry.bathroom_consumption + waterEntry.kitchen_consumption;
		var totalConstEl = $("<td>").html(totalConsumptionValue + ' m&#179;' + (!editable ? ' &#10003;': ' '));
		if ( editable ) {
			totalConstEl.append(editBtn);
			editBtn.attr('data-bathroom-consumption', waterEntry.bathroom_consumption);
			editBtn.attr('data-kitchen-consumption', waterEntry.kitchen_consumption);
			editBtn.attr('data-month', waterEntry.month);
			editBtn.attr('data-comment', waterEntry.comment);
		}
		return totalConstEl;
	}

	$.each(waterConsumptionEntriesReversed, function (key, item) {
		var tr = $("<tr>").appendTo(tableBody);
		var editable = item.editable;
		$("<td>").text(item.month).appendTo(tr);
		var totalConsEl = getTotalConsumptionEl(item, editable);
		totalConsEl.appendTo(tr);
	});
}
