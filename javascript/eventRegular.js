$(document).ready(function () {
    populateComplaintsTable();
    addComplaintButtonEventHandler();
    saveComplaintEventHandler();
    confirmationDialogEventHandler();
});

function saveComplaintEventHandler() {
    $("#save-complaint").click(function () {
        var subject = $("#subject").val();
		var date = $("#date").val();
		var loc = $("#loc").val();
        var description = $("#description").val();
        if (isComplaintValid(subject, date,loc, description) == true) {
            saveComplaint(subject, date,loc, description);
            $("#complaintModal").modal("hide");
            successNotification();
        }
        else {
            $("#complaint-error-message").text("All fields must be completed!");
        }
    });
}

function successNotification() {
    var body = $("body");
    var div = $("<div>").addClass("notification-div").appendTo(body);
    $("<strong>").text("Success!").appendTo(div);
    div.addClass("alert alert-success alert-dismissable").text("You successfully added a new complaint");

    setTimeout(function () {
        $(".notification-div").fadeOut().empty();
    }, 3000);
}

function addComplaintButtonEventHandler() {
    $("#btn-add-complaint").click(function () {
        clearModal();
    });
}

function confirmationDialogEventHandler() {
    $("#btn-yes-confirmation").click(function () {
        $("#complaintModal").modal("hide");
    });
}

function saveComplaint(subject, date, loc, description) {
    var complaint = {
        subject: subject,
        date: date,
		loc: loc,
        description: description,
        user: getLoggedUserName()
    }
    addComplaint(complaint);
    populateComplaintsTable();
}

function clearModal() {
    $("#complaint-error-message").text("");
    $("#description").val("");
    $("#subject").val("");
	$("#date").val("");
	$("#loc").val("");
    $("#complaint-type").removeClass("error-border");
    $("#subject").removeClass("error-border");
    $("#description").removeClass("error-border");
}

function isComplaintValid(subject, date, location, description) {
    var returnedValue = true;
    if (subject.length === 0) {
        $("#subject").addClass("error-border");
        returnedValue = false;
    }
    else {
        $("#subject").removeClass("error-border");
    }
	 if (date.length === 0) {
        $("#date").addClass("error-border");
        returnedValue = false;
    }
    else {
        $("#date").removeClass("error-border");
    }
	 if (loc.length === 0) {
        $("#loc").addClass("error-border");
        returnedValue = false;
    }
    else {
        $("#loc").removeClass("error-border");
    }
    if (description.length === 0) {
        $("#description").addClass("error-border");
        returnedValue = false;
    }
    else {
        $("#description").removeClass("error-border");
    }
    return returnedValue;
}

function populateComplaintsTable() {
    var tableBody = $("#body-complaints");
    var complaints = getAllComplaints();
    tableBody.empty();

    $.each(complaints, function (key, item) {
        var tr = $("<tr>").appendTo(tableBody);
        $("<td>").text(item.subject).appendTo(tr);
		$("<td>").text(item.date).appendTo(tr);
		$("<td>").text(item.loc).appendTo(tr);
        $("<td>").text(item.description).appendTo(tr);
        $("<td>").text(item.user).appendTo(tr);
    });
}
