$(document).ready(function () {
	setAnnouncementsEntries();
	populateAnnouncementsTable();
	saveAnnouncementEntryEvHandler();
	confirmationDialogEventHandler();
});


const populateAnnouncementsTable = () => {
	const tableBody = $('#body-announcements');
	tableBody.empty();

	let announcementsEntries = getAnnouncementsEntries();

	var trimByWord = function (sentence) {
		var result = sentence;
		var resultArray = result.split(" ");
		if(resultArray.length > 10){
			resultArray = resultArray.slice(0, 10);
			result = resultArray.join(" ") + "...";
		}
		return result;
	}
	
	$.each(announcementsEntries, function (key, item) {
		var tr = $("<tr>").appendTo(tableBody),
			collapseId = 'collapse-'+ key;
		$("<td>").text(item.username).appendTo(tr);
		$("<td>").html(
			function() {
				var announcement = item.announcement;
				return "<div id=\"accordion\" role=\"tablist\">\n" +
					"\t\t\t\t<div class=\"card\">\n" +
					"\t\t\t\t\t<div class=\"card-header\" role=\"tab\" id=\"headingOne\">\n" +
					"\t\t\t\t\t\t<span style=\"text-align:center\" class=\" expandControl\" data-toggle=\"collapse\" href=\"#"+collapseId + "\" aria-expanded=\"false\" aria-controls=\"collapseOne\">\n" +
					"\t\t\t\t\t\t\t<p class=\"faq-question-text\"> " + trimByWord(item.announcement_title) +
					"\t\t\t<div style=\"text-align:center\"><div class=\"glyphicon glyphicon-menu-down\"></div></div>"+ " </p> <span class=\"plus\"> </span>\n" +
					"\t\t\t\t\t\t</span>\n" +
					"\t\t\t\t\t</div>\n" +
					"\n" +
					"\t\t\t\t\t<div id=\""+ collapseId + "\" class=\"collapse\" role=\"tabpanel\" aria-labelledby=\"headingOne\" data-parent=\"#accordion\">\n" +
					"\t\t\t\t\t\t<div class=\"card-body\">\n" +
					"\t\t\t\t\t\t\t " + announcement + " \n" +
					"\t\t\t\t\t\t</div>\n" +
					"\t\t\t\t\t</div>\n" +
					"\t\t\t\t</div>\n" + 
					"\t\t\t</div>";
			}

		).appendTo(tr);
		$("<td>").text(item.date).appendTo(tr);
	});
};

const addAnnouncementError = () => {
	$("#announcement-message").addClass("error-border");
}
const isFieldValid = ( ev ) => {
	if( ev.currentTarget.value.length == 0 ) {
		$("#"+ ev.currentTarget.getAttribute('id')+"").addClass('error-border');
	}
	if( ev.currentTarget.value.length > 0 ) {
		$("#"+ ev.currentTarget.getAttribute('id')+"").removeClass('error-border');
	}
}

const isAnnouncementEntryValid = ( announcementEntry ) => {
	var isValid = true;
	if ( announcementEntry.announcement.length == 0 ) {
		addAnnouncementError()
		isValid = false;
	}
	else {
		$("#announcement-message").removeClass("error-border");
	}
	if( announcementEntry.announcement_title.length === 0 ) {
		$("#announcement-title").addClass("error-border");
		isValid = false;
	}
	else {
		$("#announcement-title").removeClass("error-border");
	}
	return isValid;
}

const confirmationDialogEventHandler = () => {
	$("#btn-yes-confirmation").click(function () {
		$("#addAnnouncementModal").modal("hide");
	});
}

const saveAnnouncementEntryEvHandler = () => {
	$("#add-announcement-btn").click(() => {
		var announcementMessage = $("textarea[name='announcement-message']").val(),
			announcementTitle = $("input[name='announcement-title']").val(),
			currentUser = getLoggedUserName(),
			currentDate = moment().format('DD.MM.YYYY'),
			announcementEntry = {
				username: currentUser,
				announcement_title: announcementTitle,
				announcement: announcementMessage,
				date: currentDate,
				new: true
			};

		if( isAnnouncementEntryValid( announcementEntry ) == true ) {
			saveAnnouncementEntry( announcementEntry );
			populateAnnouncementsTable();
			$("#addAnnouncementModal").modal('toggle');
			announcementAddedSuccessfully();
			clearModal();
		}
		else {
			$("#announcement-error-message").text("All fields must be completed!");
		}
	})
}

const announcementAddedSuccessfully = ( ) => {
	var announcementContainer = $("#announcements-container");
	var div = $("<div>").addClass("notification-div").appendTo(announcementContainer);
	$("<strong>").text("Success!").appendTo(div);
	div.addClass("alert alert-success alert-dismissable").text("Announcement addition was successful");

	setTimeout(function () {
		$(".notification-div").fadeOut().empty();
	}, 3000);
}

const clearModal = () => {
	$("textarea[name='announcement-message']").val("");
	$("input[name='announcement-title']").val("");

	// $("#complaint-error-message").text("");
	// $("#description").val("");
	// $("#title").val("");
	// $("input[name='type']").prop("checked", false);
	// $("#complaint-type").removeClass("error-border");
	// $("#title").removeClass("error-border");
	// $("#description").removeClass("error-border");
}