$(document).ready(function () {
    populateComplaintsTable();
	
	deleteUserEventHandler();
    confirmationDialogEventHandler();
	
	confirmUserEventHandler();
	confirmationDialogEventHandler1();
});


function populateComplaintsTable() {
    var tableBody = $("#body-complaints");
    var complaints = getAllComplaints();

    $.each(complaints, function (key, item) {
		
        var tr = $("<tr>").appendTo(tableBody);
		var tr = $("<tr>").attr("id", item.number).appendTo(tableBody);
		var td = $("<td>").appendTo(tr);
        $("<a>").attr("type", "button").attr("class", "btn btn-success btn-approve").attr("number", item.number).attr("data-toggle", "modal").attr("data-target", "#confirmationModal1").text("✓").appendTo(td);
		var td = $("<td>").appendTo(tr);
        $("<a>").attr("type", "button").attr("class", "btn btn-danger btn-remove").attr("number", item.number).attr("data-toggle", "modal").attr("data-target", "#confirmationModal").text("X").appendTo(td);
		$("<td>").text(item.meeting_name).appendTo(tr);
        $("<td>").text(item.description).appendTo(tr);
		
		$("<td>").text(item.loc).appendTo(tr);        
        $("<td>").text(item.user).appendTo(tr);
    });
}


function deleteUserEventHandler() {
    $(".btn-remove").click(function () {
        var number = $(this).attr("number");
        localStorage.setItem("number", number);
    });
}

function confirmationDialogEventHandler() {
    $("#btn-yes-confirmation").click(function () {
        var number = localStorage.getItem("number");
        $("#" + number).hide();
        successDeleteNotification();
    });
}

function successDeleteNotification() {
    var body = $("body");
    var div = $("<div>").addClass("notification-div").appendTo(body);
    $("<strong>").text("Success!").appendTo(div);
    div.addClass("alert alert-success alert-dismissable").text("You successfully deny the meeting!");

    setTimeout(function () {
        $(".notification-div").fadeOut().empty();
    }, 3000);
}






function confirmUserEventHandler() {
    $(".btn-approve").click(function () {
        var number = $(this).attr("number");		
        localStorage.setItem("number", number);
    });
}

function confirmationDialogEventHandler1() {
    $("#btn-yes-confirmation1").click(function () {
        var number = localStorage.getItem("number");
		$("#" + number).hide();
        successDeleteNotification1();
    });
}

function successDeleteNotification1() {
    var body = $("body");
    var div = $("<div>").addClass("notification-div").appendTo(body);
    $("<strong>").text("Success!").appendTo(div);
    div.addClass("alert alert-success alert-dismissable").text("You successfully approve the meeting!");

    setTimeout(function () {
        $(".notification-div").fadeOut().empty();
    }, 3000);
}