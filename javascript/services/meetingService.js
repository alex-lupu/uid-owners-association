var meetings = [{
    subject: "Petition",
    date: "01.02.2018 11.00",
    location: "Conference room",
    description: "I would like to meet in order to give you to sign the petition we need to receive funds from the City Hall."
}, {
    subject: "Absence of elevator",
    date: "02.02.2018 18.00",
    location: "Conference room",
    description: "Have you notice that the majority of the buildings have elevator? I propose to meet and to discuss solutions."
}, {
    subject: "Christmas presents for children",
    date: "23.12.2018 13.00",
    location: "Conference room",
    description: "I propose to meet to speak about Christmas presents for children."
}];

function addMeeting(meeting) {
    meetings.push(meeting);
}

function getAllMeetings() {
    return meetings;
}
