let waterConsumptionDataByMonths = [
	{
		month: "May",
		bathroom_consumption: 150000,
		kitchen_consumption: 7343,
		editable: false,
	},
	{
		month: "June",
		bathroom_consumption: 173000,
		kitchen_consumption: 957,
		editable: false,
	},
	{
		month: "July",
		bathroom_consumption: 140000,
		kitchen_consumption: 1421,
		editable: false,
	},
	{
		month: "August",
		bathroom_consumption: 180320,
		kitchen_consumption: 320,
		editable: false,
	},
	{
		month: "September",
		bathroom_consumption: 150000,
		kitchen_consumption: 3300,
		editable: false,
	},
	{
		month: "October",
		bathroom_consumption: 150000,
		kitchen_consumption: 3300,
		editable: false,
	},
	{
		month: "November",
		bathroom_consumption: 120000,
		kitchen_consumption: 3000,
		editable: true,
	}
];

// ES6 function syntax
const addWaterConsumptionEntry = entry => {
	waterConsumptionDataByMonths.push(entry)
}

const writeDataToLocalStorage = () => {
	console.log( waterConsumptionDataByMonths );
	localStorage.setItem('waterConsumptionData', JSON.stringify(waterConsumptionDataByMonths))
}

const updateWaterConsumptionEntry = entry => {
	const waterConsumptionToUpdate = getWaterEntryByMonth( entry.month );
	const index = waterConsumptionDataByMonths.indexOf(waterConsumptionToUpdate);
	// remove old one
	waterConsumptionDataByMonths.splice(index, 1);
	// add new one
	waterConsumptionDataByMonths.push(entry);
	// write to storage - hacky but it's the fastest i could think of rn
	writeDataToLocalStorage();
}

const isValidNumber = (evt) => {
	// this lets only numbers to be introduced in the field
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if ( (charCode > 31 && charCode < 48) || charCode > 57) {
		return false;
	}
	return true;
}

const setWaterConsumptionEntries = ( ) => {
	// delete if accidentally there was something else before
	// merge mockup local ones with the ones in local storage
	_.merge(waterConsumptionDataByMonths, getWaterConsumptionEntries());
	// localStorage.removeItem('waterConsumptionData');
	// Put the object into storage
	writeDataToLocalStorage();
	// Retrieve the object from storage
}



const getWaterEntryByMonth = ( month ) => {
	return getWaterConsumptionEntries().filter( entry => entry.month === month );
};

const getWaterConsumptionEntries = () => {
	return JSON.parse(localStorage.getItem('waterConsumptionData'));
};