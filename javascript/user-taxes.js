$(document).ready(function () {
	populateUserTaxes();
	submitPaymentEventHandler();
	confirmationDialogEventHandler();
});


function populateUserTaxes(){
	var taxContainer = $("#taxes-group");
	taxContainer.html("");
	var loggedUser = localStorage.getItem("loggedUserName");
	$.each(getTaxesOf(loggedUser), function (key, item) {
		var taxDiv = $("<div>");
		var alex = $("<a>").attr("data-toggle", "collapse")
			.attr("data-parent", "#taxes-group")
			.attr("href", "#collapse"+key);

		taxDiv.attr("class", "panel panel-default");

		alex.append(taxDiv);

		if(item.status === "NO"){
			taxDiv.attr("style", "background-color: lightcoral");
		} else {
			taxDiv.attr("style", "background-color: lightgreen");
		}
		
		var div = $("<div>");
		div.attr("class", "panel-heading");

		var h4 = $("<h4>");
		h4.attr("class", "pannel-title");

		var text = $("<a>");
		text.attr("data-toggle", "collapse")
			.attr("data-parent", "#taxes-group")
			.attr("href", "#collapse"+key)
			.text(item.month);

		h4.append(text);
		div.append(h4);
		taxDiv.append(div);

		var expanded = $("<div>");
		expanded.attr("id", "collapse"+key)
				.attr("class", "panel-collapse collapse")
				.attr("aria-expanded", "false");

		var pannelBody = $("<div>");
		div.attr("class", "pannel-body");
		if(item.status === "NO") {
			var button = $("<a>").attr("type", "button")
								.attr("class", "btn btn-info")
								.attr("data-toggle", "modal")
								.attr("data-target", "#paymentModal")
								.attr("style", "float:right;position: relative;right: 10px;bottom: -105px;")
								.text("PAY");
			pannelBody.append(button);
			button.click(function(){
				localStorage.setItem("tax-for-payment", key);
			});
		}

		var water = $("<p>");
		water.text("Water: " + item.water);
		pannelBody.append(water);
		var perCapita = $("<p>");
		perCapita.text("Per capita: " + item.perCapita);
		pannelBody.append(perCapita);
		var illumination = $("<p>");
		illumination.text("Water: " + item.illumination);
		pannelBody.append(illumination);
		var cleaning = $("<p>");
		cleaning.text("Water: " + item.cleaning);
		pannelBody.append(cleaning);
		var total = $("<p>");
		total.attr("style", "font-weight:bold");
		total.text("Total: " + (item.water + item.perCapita + item.illumination + item.cleaning));
		pannelBody.append(total);
		expanded.append(pannelBody);
		taxDiv.append(expanded);

		taxContainer.append(alex);
	});
}


function submitPaymentEventHandler(){
	$("#payButton").click(function(){
		if(isPaymentInfoValid()){
			payTax(localStorage.getItem("loggedUser"), localStorage.getItem("tax-for-payment"));

			$("#paymentModal").modal("hide");
			populateUserTaxes();
		}
	})
}

function isPaymentInfoValid() {
	//TODO;
	var valid = true;
	var cardNumber = $("#card-number").val();
	var cardDate = $("#card-date").val();
	var cardCode = $("#card-code").val();
	var cardName = $("#card-name").val();
	if(!(/\d\d\d\d-\d\d\d\d-\d\d\d\d-\d\d\d\d/.test(cardNumber))) {
		$("#card-number").addClass("error-border");
		$("#tax-err-message").text("Card number should be xxxx-xxxx-xxxx-xxxx");
		valid = false;
	} else {
		$("#card-number").removeClass("error-border");
		$("#tax-err-message").text("");
	}

	if(!(/\d\d\/\d\d/.test(cardDate))) {
		$("#card-date").addClass("error-border");
		$("#tax-err-message").text("Card date should be xx/xx");
		valid = false;
	} else {
		$("#card-date").removeClass("error-border");
		$("#tax-err-message").text("");
	}
	if(!(/\d\d\d/.test(cardCode))) {
		$("#card-code").addClass("error-border");
		$("#tax-err-message").text("Code should be xxx");
		valid = false;
	} else {
		$("#card-code").removeClass("error-border");
		$("#tax-err-message").text("");
	}
	if(!(/[A-Z]+/.test(cardName))) {
		$("#card-name").addClass("error-border");
		$("#tax-err-message").text("Please enter the name on the card!");
		valid = false;
	} else {
		$("#card-name").removeClass("error-border");
		$("#tax-err-message").text("");
	}

	return valid;
}

function confirmationDialogEventHandler() {
    $("#btn-yes-confirmation").click(function () {
        $("#paymentModal").modal("hide");
    });
}