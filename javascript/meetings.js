$(document).ready(function () {
    populateMeetingsTable();
    addMeetingButtonEventHandler();
    saveMeetingEventHandler();
    confirmationDialogEventHandler();
});

function saveMeetingEventHandler() {
    $("#save-meeting").click(function () {
        var subject = $("#subject").val();
        var location = $("#location").val();
        var description = $("#description").val();
        if (isMeetingValid(subject, location, description) == true) {
            $("#meetingModal").modal("hide");
            successNotification();
        }
        else {
            $("#add-meeting-error-message").text("All fields must be completed!");
        }
    });
}

function successNotification() {
    var body = $("body");
    var div = $("<div>").addClass("notification-div").appendTo(body);
    $("<strong>").text("Success!").appendTo(div);
    div.addClass("alert alert-success alert-dismissable").text("Your request of meeting was sent to the administrator. It will be approved or denied.");

    setTimeout(function () {
        $(".notification-div").fadeOut().empty();
    }, 10000);
}

function addMeetingButtonEventHandler() {
    $("#btn-add-meeting").click(function () {
        clearModal();
    });
}

function confirmationDialogEventHandler() {
    $("#btn-yes-confirmation").click(function () {
        $("#meetingModal").modal("hide");
    });
}

function clearModal() {
    $("#add-meeting-error-message").text("");
    $("#description").val("");
    $("#subject").val("");
    $("#location").val("");
    $("#location").removeClass("error-border");
    $("#description").removeClass("error-border");
    $("#subject").removeClass("error-border");
}

function isMeetingValid(subject, location, description) {
    var returnedValue = true;
    if (subject.length === 0) {
        $("#subject").addClass("error-border");
        returnedValue = false;
    }
    else {
        $("#subject").removeClass("error-border");
    }
    if (description.length === 0) {
        $("#description").addClass("error-border");
        returnedValue = false;
    }
    else {
        $("#description").removeClass("error-border");
    }
    if (location.length === 0) {
        $("#location").addClass("error-border");
        returnedValue = false;
    }
    else {
        $("#location").removeClass("error-border");
    }
    return returnedValue;
}

function populateMeetingsTable() {
    var tableBody = $("#body-meetings");
    var meetings = getAllMeetings();
    tableBody.empty();

    $.each(meetings, function (key, item) {
        var tr = $("<tr>").appendTo(tableBody);
        $("<td>").text(item.subject).appendTo(tr);
        $("<td>").text(item.description).appendTo(tr);
        $("<td>").text(item.location).appendTo(tr);
        $("<td>").text(item.date).appendTo(tr);
        tr.attr("id", "meeting"+key);
        
        tr.click(function(){
            localStorage.setItem("meetingResponseId", "meeting"+key);
            $("#attendance-modal-title").text("Do you wish to attend the meeting: " + item.subject + "?");
            $("#attendanceModal").modal("show");

            $("#btn-do-attend").click(function(){
                $("#"+localStorage.getItem("meetingResponseId")).addClass("confirmed");
                $("#"+localStorage.getItem("meetingResponseId")).removeClass("infirmed");
                $("#attendanceModal").modal("hide");
            });
            $("#btn-no-attend").click(function(){
                $("#"+localStorage.getItem("meetingResponseId")).addClass("infirmed");
                $("#"+localStorage.getItem("meetingResponseId")).removeClass("confirmed");
                $("#attendanceModal").modal("hide");
            });
        });
    });
}