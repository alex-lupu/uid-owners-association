$(document).ready(function () {
    login();
});


function login() {

    $("#btn-login").click(function () {

        var username = $("#username").val();
        var password = $("#password").val();
        var loggedUser = findByUsernameAndPassword(username, password);
        if (loggedUser == null)
            $("#error-login").text("Username or password are invalid!");
        else {
            localStorage.setItem("loggedUser", loggedUser.number);
            if (loggedUser.role == "admin") {
                $(this).attr("href", "adminPage.html");
            }
            else {
                $(this).attr("href", "userPage.html");
            }
        }
    });

}