$(document).ready(function () {
    var blockId = localStorage.getItem("blockId");
    populateUsersTable(blockId);
    addUserButtonEventHandler();
    deleteUserEventHandler();
    saveUserEventHandler();
    confirmationDialogEventHandler();
});

function saveUserEventHandler() {
    $("#save-user").click(function () {
        var newUsername = $("#new-username").val();
        var userApartmentNumber = $("#ap-number").val();
        var tempPassword = $("#temp-password").val();

        if (isUserValid(newUsername, userApartmentNumber, tempPassword) == true) {
            saveUser(newUsername, userApartmentNumber, tempPassword);
            $("#userModal").modal("hide");
            successNotification();
        }
        else {
            $("#add-user-error-message").text("All fields must be completed!");
        }
    });
}

function saveUser(newUsername, userApartmentNumber, tempPassword) {
    var blockId = localStorage.getItem("blockId");
    var user = {
        username: newUsername,
        password: tempPassword,
        number: userApartmentNumber,
        role: "regular",
        taxes: [],
        blockId: blockId
    }
    addUser(user);
    populateUsersTable(blockId);
}

function successNotification() {
    var body = $("body");
    var div = $("<div>").addClass("notification-div").appendTo(body);
    $("<strong>").text("Success!").appendTo(div);
    div.addClass("alert alert-success alert-dismissable").text("You successfully added a new user");

    setTimeout(function () {
        $(".notification-div").fadeOut().empty();
    }, 3000);
}

function isUserValid(newUsername, userApartmentNumber, tempPassword) {
    var returnedValue = true;
    if (newUsername.length === 0) {
        $("#new-username").addClass("error-border");
        returnedValue = false;
    }
    else {
        $("#new-username").removeClass("error-border");
    }
    if (userApartmentNumber.length === 0) {
        $("#ap-number").addClass("error-border");
        returnedValue = false;
    }
    else {
        $("#ap-number").removeClass("error-border");
    }
    if (tempPassword.length === 0) {
        $("#temp-password").addClass("error-border");
        returnedValue = false;
    }
    else {
        $("#temp-password").removeClass("error-border");
    }
    console.log(returnedValue);
    return returnedValue;
}

function addUserButtonEventHandler() {
    $("#btn-add-user").click(function () {
        clearModal();
    });
}

function clearModal() {
    $("#add-user-error-message").text("");
    $("#new-username").val("");
    $("#ap-number").val("");
    $("#temp-password").val("");
    $("#new-username").removeClass("error-border");
    $("#ap-number").removeClass("error-border");
    $("#temp-password").removeClass("error-border");
}

function populateUsersTable(blockId) {
    var tableBody = $("#body-users");
    tableBody.empty();
    var usersFromBlock = getUsersFromBlock(blockId);

    $.each(usersFromBlock, function (key, item) {
        var tr = $("<tr>").attr("id", item.number).attr("user", item.username).appendTo(tableBody);
        $("<td>").text(item.username).appendTo(tr);
        $("<td>").text("Apartment nr " + item.number).appendTo(tr);
        var td = $("<td>").appendTo(tr);
        $("<a>").attr("type", "button").attr("class", "btn btn-danger btn-remove").attr("number", item.number).attr("data-toggle", "modal").attr("data-target", "#confirmationModal").text("X").appendTo(td);
    });
}

function deleteUserEventHandler() {
    $(".btn-remove").click(function () {
        var number = $(this).attr("number");
        localStorage.setItem("deleteUserNr", number);
    });
}

function confirmationDialogEventHandler() {
    $("#btn-yes-confirmation").click(function () {
        var deleteUserNr = localStorage.getItem("deleteUserNr");
        $("#" + deleteUserNr).hide();
        successDeleteNotification();
    });

    $("#btn-yes-confirmation-add").click(function () {
        $("#userModal").modal("hide");
    });
}

function successDeleteNotification() {
    var body = $("body");
    var div = $("<div>").addClass("notification-div").appendTo(body);
    $("<strong>").text("Success!").appendTo(div);
    div.addClass("alert alert-success alert-dismissable").text("You successfully deleted the user!");

    setTimeout(function () {
        $(".notification-div").fadeOut().empty();
    }, 3000);
}

function searchFunction() {
    var input, filter, table, rows, username, i;
    input = $("#search-input").val();
    filter = input.toUpperCase();

    table = $("#body-users");
    rows = table.find('tr');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < rows.length; i++) {
        username = $(rows[i]).attr("user");

        if (username.toUpperCase().indexOf(filter) > -1) {
            rows[i].style.display = "";
        } else {
            rows[i].style.display = "none";
        }
    }
}